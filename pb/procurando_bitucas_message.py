#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging.handlers
import os
import argparse
import configparser
import sys
import asyncio
import time
import telegram


BITUCAS_LOGGING_LEVEL = int(os.getenv("BITUCAS_LOGGING_LEVEL", 10))
logger = logging.getLogger(__name__)
logger.setLevel(BITUCAS_LOGGING_LEVEL)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(BITUCAS_LOGGING_LEVEL)
S_HANDLER.setFormatter(formatter)
logger.addHandler(S_HANDLER)


def is_under_maintenance():
    return os.getenv("BITUCAS_UNDER_MAINTENANCE", False)


def is_dry_run():
    return os.getenv("BITUCAS_DRY_RUN", False)


def config_file_path():
    return os.getenv("PB_CONFIG", "/etc/bitucas.conf")


def config_file():
    config_path = config_file_path()
    if not os.path.exists(config_path):
        raise ValueError("Could not obtain configuration file path.")
    config = configparser.ConfigParser()
    config.read(config_path)
    return config


def get_chat_id():
    token = os.getenv("BITUCAS_CHAT_ID", None)
    if not token:
        config = config_file()
        token = config["telegram"]["chat_id"]
    return token


def get_telegram_token():
    return os.getenv("TELEGRAM_TOKEN", config_file()['telegram']['token'])


def show_configuration():
    telegram_token = str(get_telegram_token())[:8]
    logger.info(f"CONFIG FILE: {config_file_path()}")
    logger.info(f"TELEGRAM TOKEN: {telegram_token}")
    logger.info(f"CHAT ID: {get_chat_id()}")
    logger.info(f"UNDER MAINTENANCE: {is_under_maintenance()}")
    logger.info(f"DRY RUN: {is_dry_run()}")


async def run():
    max_retry = 5
    retry_count = 0


    parser = argparse.ArgumentParser(prog='BitucasMessenger', description='Send messages to PB Telegram')
    parser.add_argument('message', type=str, help='Message to be sent')
    args = parser.parse_args()

    show_configuration()

    if is_under_maintenance():
        logging.warning("Procurando Bitucas is under maintenance. Exiting now.")
        sys.exit(503)

    while retry_count < max_retry:
        try:
            bot = telegram.Bot(token=get_telegram_token())
            if not is_dry_run():
                try:
                    async with bot:
                        await bot.send_message(chat_id=get_chat_id(), text=args.message)
                except Exception as error:
                    logger.error(f"Could not post message: {error}")
                    retry_count += 1
                    time.sleep(3)
                    continue
        except Exception as error:
            logger.error(f"Could not initialize Telegram: {error}")
            retry_count += 1
            time.sleep(3)
            continue
        break


def main():
    asyncio.run(run())


if __name__ == '__main__':
    main()
